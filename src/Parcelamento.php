<?php
namespace Webcria\Webcriapay;

use Webcria\Webcriapay\Base;

class Parcelamento extends Base
{
    CONST COMPRADOR = 'with_interest';
    CONST LOJISTA = 'interest_fee';

    public $mode;
    public $number_installments;
    public $description;


    public function toArray()
    {   
        return get_object_vars($this);
    }

}
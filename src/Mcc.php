<?php

namespace Webcria\Webcriapay;

use Webcria\Webcriapay\Base;

class Mcc extends Base
{
    use ItemList;

    protected $http;
    protected $codes = [];

    public function __construct(Http $http)
    {
        $this->http = $http;
    }

    public function list($limit = 20, $sort = 'time-descending', $offset = 0)
    {
        $response = $this->http->get('?limit=' . $limit . '&sort=' . $sort . '&offset=' . $offset);
        $body = json_decode($response->getBody());
        $this->setObject($body);
    }

    private function setObject($object)
    {
        foreach($object as $key => $value) {
            if (property_exists($this, $key)) {
               $this->$key = $value;
            } else if ($key == 'items') {
                foreach ($value as $item) {
                    $codes[] = $item;
                }
            }
        }
    }

}
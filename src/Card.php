<?php

namespace Webcria\Webcriapay;

use Webcria\Webcriapay\Base;

use Webcria\Webcriapay\Owner;
use Webcria\Webcriapay\Address;
use Webcria\Webcriapay\Business;
use Webcria\Webcriapay\Comprador;

use Webcria\Webcriapay\Http;

class Card extends Base
{
    /**
     * Card Json Response from token request
     * {
        "id": "ac14e45795fc42d7a1b0fe9454f929ae",
        "resource": "token",
        "used": false,
        "type": "card",
        "card": {
        "id": "c7dd808887ec49edb858184b3f98aa21",
        "resource": "card",
        "description": null,
        "card_brand": "Visa",
        "first4_digits": "4111",
        "expiration_month": "12",
        "expiration_year": "2023",
        "holder_name": "Rogers Sampaio Santos",
        "is_active": false,
        "is_valid": true,
        "is_verified": false,
        "customer": null,
        "fingerprint": "57c5a697749dc0baaed49f44d36381ac75724e9ab8bc64c4ce91e0bcd284ee72",
        "address": null,
        "verification_checklist": {
        "postal_code_check": "unchecked",
        "security_code_check": "unchecked",
        "address_line1_check": "unchecked"
        },
        "metadata": {},
        "uri": "/v1/marketplaces/2c75c5622aff4ba087e0e50c8d6d2919/cards/c7dd808887ec49edb858184b3f98aa21",
        "created_at": "2020-07-11T13:42:29+00:00",
        "updated_at": "2020-07-11T13:42:29+00:00"
        },
        "uri": "/v1/marketplaces/2c75c5622aff4ba087e0e50c8d6d2919/tokens/ac14e45795fc42d7a1b0fe9454f929ae",
        "created_at": "2020-07-11T13:42:29+00:00",
        "updated_at": "2020-07-11T13:42:29+00:00"
        }
    */
    
    protected   $token;
    protected   $id;
    protected   $resource;
    protected	$description;
    protected	$card_brand;
    protected	$first4_digits;
    protected	$fingerprint;
    protected	$customer;
    protected	$expiration_month;
    protected   $expiration_year;
    protected   $holder_name;
    protected	$is_active;
    protected   $is_valid;
    protected	$is_verified;
    protected	$address;
    // TODO make an class, json object for now
    protected	$verification_checklist;
    protected	$metadata;
    protected	$uri;
    protected	$created_at;
    protected	$updated_at;


    protected $http;
    
    /**
     * __construct
     *
     * @param  Http $http
     * @return void
     */
    public function __construct(Http $http)
    {
        $this->http = $http;
    }
    /**
     * deserialize
     *
     * @param  mixed $data
     * @return void
     */
    public function deserialize($data)
    {   
        foreach ($data as $property => $value) {

            if(property_exists($this, $property)) {
                $this->$property = $value;
            }
        }
    }

    public function createCard($tokenResponse) 
    {
        $this->token = $tokenResponse->id;
        $this->deserialize($tokenResponse->card);
        
        return $this;
    }

    public function associar($comprador_id)
    {
        $data = [
            'token'         => $this->token,
            'customer'      => $comprador_id
        ];

        $response = $this->http->post('cards', $data);
        $body = json_decode($response->getBody());
        
        $this->deserialize($body);

        return $this;
    }

    public function get($card_id)
    {   
        $this->id = $card_id;

        $response = $this->http->get('cards/' . $this->id);
        $body = json_decode($response->getBody());

        $this->deserialize($body);

        return $this;
    }

    // TODO Update Cartão
    // TODO Delete Cartão
    
    
}
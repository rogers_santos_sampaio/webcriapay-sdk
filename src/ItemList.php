<?php

namespace Webcria\Webcriapay;

trait ItemList
{
    public $resource;
    public $uri;
    public $has_more;
    public $limit;
    public $total_pages;
    public $page;
    public $offset;
    public $total;
    public $query_count;

}
<?php

namespace Webcria\Webcriapay;

use Webcria\Webcriapay\Base;

use Webcria\Webcriapay\ItemList;
use Webcria\Webcriapay\Transaction;

class Recebiveis extends Base
{
    use ItemList;

    protected $http;
    protected $items = [];

    public function __construct(Http $http)
    {
        $this->http = $http;
    }

    public function listarRecebiveis($transaction, $limit = 20, $sort = 'time-descending', $offset = 0)
    {
        $response = $this->http->get('transactions/' . $transaction . '/receivables?limit='.$limit.'&sort='.$sort.'&offset='.$offset);
        $body = json_decode($response->getBody());
        $this->setObject($body);
    }

    private function setObject($object)
    {
        foreach($object as $key => $value) {
            if (property_exists($this, $key)) {
               $this->$key = $value;
            }
        }
    }

}
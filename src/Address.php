<?php 

namespace Webcria\Webcriapay;

use Webcria\Webcriapay\Base;

class Address extends Base
{
    public $line1;
    public $line2 = '';
    public $line3 = '';
    public $neighborhood;
    public $city;
    public $state;
    public $postal_code;
    public $country_code;

    public function toArray()
    {   
        return get_object_vars($this);
    }

}
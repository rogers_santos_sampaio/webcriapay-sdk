<?php

namespace Webcria\Webcriapay;

use Exception;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\ClientException;

/**
 * Webcriapay HTTP Client
 * processes Http requests using guzzle
 */
class Http
{

    private $client;

    private $key;

    private $marketplace_id;

    public function __construct($key, $marketplace_id, $version = 'v1', $mcc = false, $upload = false)
    {   

        $this->key = $key;
        $this->marketplace_id = $marketplace_id;
        $this->setClient($version, $mcc, $upload);
    }

    private function setClient($version, $mcc = false, $upload = false)
    {
        $base_uri = 'https://api.zoop.ws/' . $version . '/marketplaces/' . $this->marketplace_id . '/';

        if ($mcc)
            $base_uri = 'https://api.zoop.ws/' . $version . '/merchant_category_codes';
        
        if (!$upload) {
            $this->client = new HttpClient([
                // Base URI is used with relative requests
                'base_uri' => $base_uri,
                // You can set any number of default request options.
                'timeout'  => 200.0,
                'headers' => [
                    'Accept' => 'application/json',
                    'Authorization' => 'Basic ' . $this->key,
                    'Content-Type' => 'application/json'
                ]
            ]);
        } else {
            $this->client = new HttpClient([
                // Base URI is used with relative requests
                'base_uri' => $base_uri,
                // You can set any number of default request options.
                'timeout'  => 200.0,
                'headers' => [
                    'Authorization' => 'Basic ' . $this->key
                ]
            ]);
        }
    }

    public function get($action)
    {
        try {
            $response = $this->client->get($action);
            return $response;
        } catch(ClientException $e) {
            throw new WebcriapayException($e->getMessage(), $e->getCode(), $e, $e->getRequest(), $e->getResponse());
        } catch(\Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function post($action, $data)
    {   
        try {
            var_dump(json_encode($data));
            $response = $this->client->post($action, ['json' => $data]);
            return $response;
        } catch(ClientException $e) {
            throw new WebcriapayException($e->getMessage(), $e->getResponse()->getStatusCode(), $e, $e->getRequest(), $e->getResponse());
        } catch(\Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode(), $e);
        }
        
    }

    public function put($action, $id, $data)
    {
        try {
            $response = $this->client->put($action . '/' . $id, ['json' => $data]);
            return $response;
        } catch(ClientException $e) {
            throw new WebcriapayException($e->getMessage(), $e->getResponse()->getStatusCode(), $e, $e->getRequest(), $e->getResponse());
        } catch(\Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function delete($action, $id)
    {
        try {
            $response = $this->client->delete($action . '/' . $id);
            return $response;
        } catch(ClientException $e) {
            throw new WebcriapayException($e->getMessage(), $e->getResponse()->getStatusCode(), $e, $e->getRequest(), $e->getResponse());
        } catch(\Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

}
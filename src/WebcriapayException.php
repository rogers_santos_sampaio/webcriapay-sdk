<?php

namespace Webcria\Webcriapay;

use Exception;

class WebcriapayException extends Exception
{

    protected $code;
    protected $request;
    protected $response;

    public function __construct($message, $code = 0, Exception $previous = null, $request = null, $response = null) {
        $this->code = $code;
        $this->request = $request;
        $this->response = $response;
        parent::__construct($message, $code, $previous);
    }

    public function getResponse() {
        return $this->response;
    }

    public function getRequest() {
        return $this->request;
    }

    public function getStatusCode() {
        return $this->code;
    }
    
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}
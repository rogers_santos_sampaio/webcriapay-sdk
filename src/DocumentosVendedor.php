<?php

namespace Webcria\Webcriapay;

use Webcria\Webcriapay\Base;

use Webcria\Webcriapay\Owner;
use Webcria\Webcriapay\Address;
use Webcria\Webcriapay\Business;

class DocumentosVendedor extends Vendedor
{

    protected $http;
    protected $vendedores = [];

    public function __construct(Http $http)
    {
        $this->http = $http;
    }

    public function listSellerDocuments()
    {
        
    }

    public function sendSellerDocuments($id, $file, $category)
    {

        $imgData = base64_encode(file_get_contents($file));

        // Format the image SRC:  data:{mime};base64,{data};
        $src = 'data: '.mime_content_type($file).';base64,'.$imgData;

        $data = [
            'file'      =>  $src,
            'category'  =>  $category
        ];

        $response = $this->http->post('sellers/' . $id . '/documents', $data);
        $body = json_decode($response->getBody());
        $this->created_at = $body->created_at;
        $this->updated_at = $body->updated_at;
        $this->type = 'individual';
    }

    public function updateSellerDocuments()
    {
        
    }

}
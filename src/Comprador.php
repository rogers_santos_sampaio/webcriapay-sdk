<?php

namespace Webcria\Webcriapay;

use Webcria\Webcriapay\Base;

use Webcria\Webcriapay\Address;

use Webcria\Webcriapay\Http;

class Comprador extends Base
{

    public $parent_seller;
    public $id;
    public $first_name;
    public $last_name;
    public $email;
    public $phone_number;
    public $taxpayer_id;
    public $address;
    public $birthdate;
    public $description;
    public $created_at;
    public $updated_at;

    protected $http;
    
    /**
     * __construct
     *
     * @param  Http $http
     * @return void
     */
    public function __construct(Http $http)
    {
        $this->http = $http;
    }
    
    /**
     * deserialize
     *
     * @param  mixed $data
     * @return void
     */
    public function deserialize($data)
    {   
        
        foreach($data as $key => $value) {
            if (property_exists($this, $key) && $key != 'address') {
                $this->$key = $value;
            } else if ($key == 'address') {
                $this->setAddress($value);
            }
        }
    }
    
    /**
     * setAddress
     *
     * @param  mixed $data
     * @param  mixed $type
     * @return void
     */
    private function setAddress($data)
    {   

        $this->address = new Address();
        foreach ($data as $property => $value) {
            if (property_exists($this->address, $property)) {
                $this->address->$property = $value;
            }
        }
    }
    
    /**
     * getBuyer
     *
     * @param  mixed $id
     * @return $this
     */
    public function get($id)
    {
        $response = $this->http->get('buyers/' . $id);
        $body = json_decode($response->getBody());
        $this->deserialize($body);

        return $this;
    }
    
    /**
     * createIndividual
     *
     * @return string $id
     */
    public function createComprador()
    {
        $data = [
            'first_name'            => $this->first_name,
            'last_name'             => $this->last_name,
            'email'                 => $this->email,
            'phone_number'          => $this->phone_number,
            'taxpayer_id'           => $this->taxpayer_id,
            'birthdate'             => $this->birthdate,
            'description'           => $this->description,
            'address'               => $this->address->toArray(),
        ];
        
        $response = $this->http->post('buyers', $data);
        $body = json_decode($response->getBody());
        $this->id = $body->id;
        $this->created_at = $body->created_at;
        $this->updated_at = $body->updated_at;

        return $this->id;

    }
    
        
    
    
    /**
     * saveIndividual
     *
     * @return void
     */
    public function saveComprador()
    {
        $data = [
            'first_name'            => $this->first_name,
            'last_name'             => $this->last_name,
            'email'                 => $this->email,
            'phone_number'          => $this->phone_number,
            'taxpayer_id'           => $this->taxpayer_id,
            'birthdate'             => $this->birthdate,
            'description'           => $this->description,
            'address'               => $this->address->toArray()
        ];

        $response = $this->http->put('buyers/', $this->id, $data);
        $body = json_decode($response->getBody());
        $this->updated_at = $body->updated_at;
    }
    
    
    /**
     * deleteComprador
     *
     * @param  mixed $id
     * @return void
     */
    public function deleteBuyer($id)
    {
        $response = $this->http->delete('buyers', $id);
        $body = json_decode($response->getBody());

        return $body;
    }

    /**
     * getBuyerBalances
     *
     * @return json object
     */
    public function getBuyerBalances()
    {
        $response = $this->http->get('buyer/' . $this->id . '/balances');
        $body = json_decode($response->getBody());

        return $body->items;
    }

    public function getBuyerBalanceHistory()
    {
        $balanceHistory = new BalanceHistory($this->http);
        $history = $balanceHistory->getBalanceHistory('buyer', $this->id);

        return $history;
    }
    
}
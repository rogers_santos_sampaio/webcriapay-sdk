<?php
namespace Webcria\Webcriapay;

use Webcria\Webcriapay\Base;

class PaymentMethod extends Base
{
    public $id;
    public $resource;
    public $card_brand;
    public $first4_digits;
    public $description;
    public $last4_digits;
    public $expiration_month;
    public $expiration_year;
    public $holder_name;
    public $is_active;
    public $is_valid;
    public $is_verified;
    public $customer;
    public $fingerprint;
    public $uri;
    public $metadata;
    public $created_at;
    public $updated_at;

    public function toArray()
    {   
        return get_object_vars($this);
    }

}
<?php
namespace Webcria\Webcriapay;

use Webcria\Webcriapay\Base;

class Owner extends Base
{
    public $first_name;
    public $last_name;
    public $email;
    public $website;
    public $phone_number;
    public $taxpayer_id;
    public $birthdate;
    public $address;


    public function toArray()
    {   
        return get_object_vars($this);
    }

}
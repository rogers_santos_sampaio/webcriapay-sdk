<?php

namespace Webcria\Webcriapay;

use Webcria\Webcriapay\Base;

use Webcria\Webcriapay\ItemList;
use Webcria\Webcriapay\Webhook;

class Webhooks extends Base
{
    use ItemList;

    protected $http;
    protected $webhooks = [];

    public function __construct(Http $http)
    {
        $this->http = $http;
    }

    public function listarWebhooks($limit = 20, $sort = 'time-descending', $offset = 0)
    {
        $response = $this->http->get('webhooks?limit='.$limit.'&sort='.$sort.'&offset='.$offset);
        $body = json_decode($response->getBody());
        $this->setObject($body);
    }

    private function setObject($object)
    {
        foreach($object as $key => $value) {
            if (property_exists($this, $key)) {
               $this->$key = $value;
            } else if ($key == 'items') {
                foreach ($value as $item) {
                    $this->setWebhook($item);
                }
            }
        }
    }

    private function setWebhook($item) 
    {
        $webhook = new Webhook($this->http);
        $webhook->deserialize($item);
        $this->webhooks[] = $webhook;
    }

}
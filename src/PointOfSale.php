<?php
namespace Webcria\Webcriapay;

use Webcria\Webcriapay\Base;

class PointOfSale extends Base
{
    public $entry_mode;
    public $identification_number;

    public function toArray()
    {   
        return get_object_vars($this);
    }

}
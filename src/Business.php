<?php
namespace Webcria\Webcriapay;

use Webcria\Webcriapay\Base;

class Business extends Base
{
    public $name;
    public $phone;
    public $email;
    public $website;
    public $description;
    public $facebook;
    public $twitter;
    public $ein;
    public $opening_date;

    public function toArray()
    {   
        return get_object_vars($this);
    }

}
<?php

namespace Webcria\Webcriapay;

class Webcriapay
{
    protected $httpClient;
    protected $key;
    protected $marketplace_id;

    public function __construct($key, $marketplace_id)
    {
        $this->key = base64_encode($key.':');
        $this->marketplace_id = $marketplace_id;
        $this->setHttpClient();
    }

    public function setHttpClient()
    {
        $this->httpClient = new Http($this->key, $this->marketplace_id);
    }

    public function vendedor() 
    {
        return new Vendedor($this->httpClient);
    } 

    public function documentosVendedor() 
    {
        $uploadClient = new Http($this->key, $this->marketplace_id, 'v1', false, true);
        return new DocumentosVendedor($uploadClient);
    } 

    public function vendedores() 
    {
        return new Vendedores($this->httpClient);
    }

    public function compradores() 
    {
        return new Compradores($this->httpClient);
    } 

    public function comprador() 
    {
        return new Comprador($this->httpClient);
    } 

    public function mcc() 
    {
        $mccHttp = new Http($this->key, $this->marketplace_id, 'v1', true);
        return new Mcc($mccHttp);
    } 

    public function transaction() 
    {
        $v2 = new Http($this->key, $this->marketplace_id, 'v2');
        return new Transaction($v2);
    } 

    public function transactions() 
    {
        $http = new Http($this->key, $this->marketplace_id);
        return new Transaction($http);
    } 

    public function card()
    {
        $http = new Http($this->key, $this->marketplace_id);
        return new Card($http);
    }

    public function webhooks()
    {
        $http = new Http($this->key, $this->marketplace_id);
        return new Webhooks($http);
    }

    public function webhook()
    {
        $http = new Http($this->key, $this->marketplace_id);
        return new Webhook($http);
    }

    public function marketplace()
    {
        $http = new Http($this->key, $this->marketplace_id);
        return new Marketplace($http);
    }
}
<?php

namespace Webcria\Webcriapay;

use Webcria\Webcriapay\Base;

use Webcria\Webcriapay\Owner;
use Webcria\Webcriapay\Address;
use Webcria\Webcriapay\Business;
use Webcria\Webcriapay\ItemList;
use Webcria\Webcriapay\Vendedor;

class Vendedores extends Base
{
    use ItemList;

    protected $http;
    public $vendedores = [];

    public function __construct(Http $http)
    {
        $this->http = $http;
    }

    public function searchVendedor($document, $type = 'cpf')
    {
        $query = '';
        if ($type == 'cpf') {
            $query = 'taxpayer_id=' . $document;
        } else {
            $query = 'ein=' . $document;
        }
        $response = $this->http->get('sellers/search?' . $query);
        $body = json_decode($response->getBody());
        $this->setObject($body);

    }

    public function listarVendedores($limit = 20, $sort = 'time-descending', $offset = 0)
    {
        $response = $this->http->get('sellers?limit='.$limit.'&sort='.$sort.'&offset='.$offset);
        $body = json_decode($response->getBody());
        $this->setObject($body);
    }

    public function toArray() {
        $arr = [];
        foreach ($this->vendedores as $vendedor) {
            $arr[] = $vendedor->toArray();
        }

        return $arr;
    }

    private function setObject($object)
    {
        foreach($object as $key => $value) {
            if (property_exists($this, $key)) {
               $this->$key = $value;
            } else if ($key == 'items') {
                foreach ($value as $item) {
                    $this->setVendedor($item);
                }
            }
        }
    }

    private function setVendedor($item) 
    {
        $vendedor = new Vendedor($this->http);
        $vendedor->deserialize($item);
        $this->vendedores[] = $vendedor;
    }

}
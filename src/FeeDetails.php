<?php
namespace Webcria\Webcriapay;

use Webcria\Webcriapay\Base;

class FeeDetails extends Base
{
    public $amount;
    public $prepaid;
    public $currency;
    public $type;
    public $description;

    public function toArray()
    {   
        return get_object_vars($this);
    }

}
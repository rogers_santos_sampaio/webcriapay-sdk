<?php

namespace Webcria\Webcriapay;

use Webcria\Webcriapay\Base;

use Webcria\Webcriapay\ItemList;
use Webcria\Webcriapay\Transaction;

class Transactions extends Base
{
    use ItemList;

    protected $http;
    protected $transactions = [];

    public function __construct(Http $http)
    {
        $this->http = $http;
    }

    public function listarTransactions($limit = 20, $sort = 'time-descending', $offset = 0)
    {
        $response = $this->http->get('transactions?limit='.$limit.'&sort='.$sort.'&offset='.$offset);
        $body = json_decode($response->getBody());
        $this->setObject($body);
    }

    public function listarVendedoresTransactions($seller_id, $limit = 20, $sort = 'time-descending', $offset = 0)
    {
        $response = $this->http->get('sellers/' . $seller_id . '/transactions?limit='.$limit.'&sort='.$sort.'&offset='.$offset);
        $body = json_decode($response->getBody());
        $this->setObject($body);
    }

    private function setObject($object)
    {
        foreach($object as $key => $value) {
            if (property_exists($this, $key)) {
               $this->$key = $value;
            } else if ($key == 'items') {
                foreach ($value as $item) {
                    $this->setTransactions($item);
                }
            }
        }
    }

    private function setTransactions($item) 
    {
        $transaction = new Transaction($this->http);
        $transaction->deserialize($item);
        $this->transactions[] = $transaction;
    }

}
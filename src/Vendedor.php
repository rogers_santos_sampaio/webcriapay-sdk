<?php

namespace Webcria\Webcriapay;

use Webcria\Webcriapay\Base;

use Webcria\Webcriapay\Owner;
use Webcria\Webcriapay\Address;
use Webcria\Webcriapay\Business;
use Webcria\Webcriapay\BalanceHistory;

use Webcria\Webcriapay\Http;

class Vendedor extends Base
{

    public $parent_seller;
    public $id;
    public $owner;
    public $business;
    public $statement_descriptor;
    public $description;
    public $business_address;
    public $owner_address;
    public $mcc;
    public $metadata;
    public $created_at;
    public $updated_at;
    public $type;
    public $status;
    public $account_balance;
    public $current_balance;
    public $merchant_code;
    public $terminal_code;
    public $show_profile_online;
    public $is_mobile;
    public $decline_on_fail_security_code;
    public $decline_on_fail_zipcode;
    public $delinquent;
    public $payment_methods;
    public $default_debit;
    public $default_credit;
    public $payout_option;

    protected $http;
    
    /**
     * __construct
     *
     * @param  Http $http
     * @return void
     */
    public function __construct(Http $http)
    {
        $this->http = $http;
    }
    
    /**
     * deserialize
     *
     * @param  mixed $data
     * @return void
     */
    public function deserialize($data)
    {   
        $type = $data->type;

        if ($type == 'individual') {
            $this->setIndividualSeller($data);
        } else {
            $this->setBusinessSeller($data);
        }
    }
    
    /**
     * setAddress
     *
     * @param  mixed $data
     * @param  mixed $type
     * @return void
     */
    private function setAddress($data, $type = 'owner_address')
    {
        $this->$type = new Address();
        foreach ($data as $property => $value) {
            if (property_exists($this->$type, $property)) {
                $this->$type->$property = $value;
            }
        }

        if($type == 'owner_address') {
            $this->owner->address = $this->$type;
        }
    }
    
    /**
     * setOwner
     *
     * @param  mixed $data
     * @return void
     */
    public function setOwner($data)
    {
        $this->owner = new Owner();
        foreach ($data as $property => $value) {

            if(property_exists($this->owner, $property) && $property !== 'address') {
                $this->owner->$property = $value;
            } else if($property == 'address') {
                $this->setAddress($value);
            }
        }
    }
    
    /**
     * setBusiness
     *
     * @param  mixed $data
     * @return void
     */
    public function setBusiness($data)
    {
        $this->business = new Business();
        foreach ($data as $property => $value) {
            $this->business->$property = $value;
        }
    }
    
    /**
     * getSeller
     *
     * @param  mixed $id
     * @return $this
     */
    public function getVendedor($id)
    {
        $response = $this->http->get('sellers/' . $id);
        $body = json_decode($response->getBody());
        $this->deserialize($body);

        return $this;
    }
    
    /**
     * createIndividual
     *
     * @return string $id
     */
    public function createIndividual()
    {
        $data = [
            'first_name'            => $this->owner->first_name,
            'last_name'             => $this->owner->last_name,
            'email'                 => $this->owner->email,
            'website'               => $this->owner->website,
            'phone_number'          => $this->owner->phone_number,
            'taxpayer_id'           => $this->owner->taxpayer_id,
            'birthdate'             => $this->owner->birthdate,
            'statement_descriptor'  => $this->statement_descriptor,
            'description'           => $this->description,
            'address'               => $this->owner_address->toArray(),
            'mcc'                   => $this->mcc,
            'metadata'              => $this->metadata,
        ];
        
        $response = $this->http->post('sellers/individuals', $data);
        $body = json_decode($response->getBody());
        $this->id = $body->id;
        $this->created_at = $body->created_at;
        $this->updated_at = $body->updated_at;
        $this->type = 'individual';

        return $this->id;

    }
    
        
    /**
     * createBusinesses
     *
     * @return string $id
     */
    public function createBusinesses()
    {
        $data = [
            'owner'                 => $this->owner->toArray(),
            'business_name'         => $this->business->name,
            'business_email'        => $this->business->email,
            'business_website'      => $this->business->website,
            'business_phone'        => $this->business->phone,
            'business_description'  => $this->business->description,
            'business_facebook'     => $this->business->facebook,
            'business_twitter'      => $this->business->twitter,
            'ein'                   => $this->business->ein,
            'business_opening_date' => $this->business->opening_date,
            'statement_descriptor'  => $this->statement_descriptor,
            'description'           => $this->description,
            'owner_address'         => $this->owner_address,
            'business_address'      => $this->business_address,
            'mcc'                   => $this->mcc,
            'metadata'              => $this->metadata,
        ];

        $response = $this->http->post('sellers/businesses/', $data);
        $body = json_decode($response->getBody());
        $this->id = $body->id;
        $this->created_at = $body->created_at;
        $this->updated_at = $body->updated_at;
        $this->type = 'business';

        return $this->id;

    }
    
    /**
     * saveIndividual
     *
     * @return void
     */
    public function saveIndividual()
    {
        $data = [
            'first_name'            => $this->owner->first_name,
            'last_name'             => $this->owner->last_name,
            'email'                 => $this->owner->email,
            'website'               => $this->owner->website,
            'phone_number'          => $this->owner->phone_number,
            'taxpayer_id'           => $this->owner->taxpayer_id,
            'birthdate'             => $this->owner->birthdate,
            'statement_descriptor'  => $this->statement_descriptor,
            'description'           => $this->description,
            'address'               => $this->owner_address->toArray(),
            'mcc'                   => $this->mcc,
            'metadata'              => $this->metadata,
        ];

        $response = $this->http->put('sellers/individuals', $this->id, $data);
        $body = json_decode($response->getBody());
        $this->updated_at = $body->updated_at;
    }
    
    /**
     * saveBusiness
     *
     * @return void
     */
    public function saveBusiness()
    {
        $data = [
            'owner'                 => $this->owner->toArray(),
            'business_name'         => $this->business->name,
            'business_email'        => $this->business->email,
            'business_website'      => $this->business->website,
            'business_phone'        => $this->business->phone,
            'business_description'  => $this->business->description,
            'business_facebook'     => $this->business->facebook,
            'business_twitter'      => $this->business->twitter,
            'ein'                   => $this->business->ein,
            'business_opening_date' => $this->business->opening_date,
            'statement_descriptor'  => $this->statement_descriptor,
            'description'           => $this->description,
            'owner_address'         => $this->owner_address,
            'business_address'      => $this->business_address,
            'mcc'                   => $this->mcc,
            'metadata'              => $this->metadata,
        ];


        $response = $this->http->put('sellers/businesses', $this->id, $data);
        $body = json_decode($response->getBody());
        $this->updated_at = $body->updated_at;
    }

        
    /**
     * deleteVendedor
     *
     * @param  mixed $id
     * @return void
     */
    public function deleteVendedor($id)
    {
        $response = $this->http->delete('sellers/', $id);
        $body = json_decode($response->getBody());

        return $body;
    }

        
    /**
     * getSellerBalances
     *
     * @return json object
     */
    public function getSellerBalances()
    {
        $response = $this->http->get('sellers/' . $this->id . '/balances');
        $body = json_decode($response->getBody());

        return $body->items;
    }

    public function getSellerBalanceHistory()
    {
        $balanceHistory = new BalanceHistory($this->http);
        $history = $balanceHistory->getBalanceHistory('seller', $this->id);

        return $history;
    }

    public function toArray()
    {
        $owner = json_encode($this->owner);
        $business = json_encode($this->business);
        $business_address = json_encode($this->business_address);
        $owner_address = json_encode($this->owner_address);

        return [
            'parent_seller' => $this->parent_seller,
            'id' => $this->id,
            'owner' => json_decode($owner, true),
            'business'  => json_decode($business, true),
            'statement_descriptor'  => $this->statement_descriptor,
            'description'  => $this->description,
            'business_address'  => json_decode($business_address, true),
            'owner_address'  => json_decode($owner_address, true),
            'mcc'  => $this->mcc,
            'metadata'  => $this->metadata,
            'created_at'  => $this->created_at,
            'updated_at'  => $this->updated_at,
            'type'  => $this->type,
            'status'  => $this->status,
            'account_balance'  => $this->account_balance,
            'current_balance'  => $this->current_balance,
            'merchant_code'  => $this->merchant_code,
            'terminal_code'  => $this->terminal_code,
            'show_profile_online'  => $this->show_profile_online,
            'is_mobile'  => $this->is_mobile,
            'decline_on_fail_security_code'  => $this->decline_on_fail_security_code,
            'decline_on_fail_zipcode'  => $this->decline_on_fail_zipcode,
            'delinquent'  => $this->delinquent,
            'payment_methods'  => $this->payment_methods,
            'default_debit'  => $this->default_debit,
            'default_credit'  => $this->default_credit,
            'payout_option'  => $this->payout_option
        ];
    }
    
    /**
     * setIndividualSeller
     *
     * @param  mixed $data
     * @return void
     */
    private function setIndividualSeller($data)
    {

        foreach($data as $key => $value) {
            if (property_exists($this, $key) && $key != 'address') {
                $this->$key = $value;
            }
        }
        
        $this->setOwner($data);
        
    }
    
    /**
     * setBusinessSeller
     *
     * @param  mixed $data
     * @return void
     */
    private function setBusinessSeller($data)
    { 
        $business = [];
        foreach($data as $key => $value) {
                
            if (property_exists($this, $key) && !is_object($value)) {
                $this->$key = $value;
            } else {
                if ($key == 'ein') {
                    $business['ein'] = $value;
                }
                else if($key == 'owner') {
                    $this->setOwner($value);
                } else if($key == 'business_address') {
                    $this->setAddress($value, 'business_address');
                } else if(preg_match('/business_/', $key, $matches)) {
                    $name = str_replace('business_', '', $key);
                    $business[$name] = $value;
                }
            }
        }

        $this->setBusiness($business);
    }
}
<?php

namespace Webcria\Webcriapay;

use Webcria\Webcriapay\Base;

use Webcria\Webcriapay\Owner;
use Webcria\Webcriapay\Address;
use Webcria\Webcriapay\Business;
use Webcria\Webcriapay\BalanceHistory;

use Webcria\Webcriapay\Http;

class Marketplace extends Base
{

    /*	
  

{
    "id": "2c75c5622aff4ba087e0e50c8d6d2919",
    "resource": "marketplace",
    "name": "WebCria Test",
    "type": "business_test",
    "description": "Marketplace de Testes WebCria",
    "is_active": true,
    "is_verified": true,
    "account_balance": "0.00",
    "current_balance": "0.00",
    "decline_on_fail_security_code": true,
    "decline_on_fail_zip_code": true,
    "support_email": "zoop@webcria.com.br",
    "phone_number": "85999959122",
    "statement_descriptor": "webcria",
    "website": "",
    "facebook": "",
    "twitter": "",
    "customer": {
        "id": "19e54a89b94d47afb00ed5a4c99380f6",
        "status": "active",
        "resource": "seller",
        "type": "business",
        "description": "Marketplace de Testes WebCria",
        "account_balance": "0.00",
        "current_balance": "0.00",
        "business_name": "Dttec Solucoes Em Tecnologia Ltda",
        "business_phone": "85999959122",
        "business_email": "zoop@webcria.com.br",
        "business_website": "",
        "business_description": "Webcria",
        "business_opening_date": "2009-06-18",
        "business_facebook": "",
        "business_twitter": null,
        "ein": "10902473000138",
        "statement_descriptor": "webcria",
        "mcc": "18",
        "business_address": {
            "line1": null,
            "line2": null,
            "line3": null,
            "neighborhood": null,
            "city": null,
            "state": null,
            "postal_code": null,
            "country_code": null
        },
        "owner": {
            "first_name": null,
            "last_name": null,
            "email": null,
            "phone_number": null,
            "taxpayer_id": null,
            "birthdate": null,
            "address": {
                "line1": null,
                "line2": null,
                "line3": null,
                "neighborhood": null,
                "city": null,
                "state": null,
                "postal_code": null,
                "country_code": null
            }
        },
        "show_profile_online": false,
        "is_mobile": false,
        "decline_on_fail_security_code": true,
        "decline_on_fail_zipcode": false,
        "delinquent": false,
        "payment_methods": null,
        "default_debit": null,
        "default_credit": null,
        "merchant_code": "012000021220006",
        "terminal_code": "GT0000CC",
        "uri": "\/v1\/marketplaces\/",
        "marketplace_id": "2c75c5622aff4ba087e0e50c8d6d2919",
        "metadata": {},
        "created_at": "2019-12-16T17:08:54+00:00",
        "updated_at": "2019-12-16T17:12:31+00:00"
    },
    "api_keys": [
        {
            "id": "dc89ed2081f2459f8bd55f2c051a7256",
            "resource": "api_key",
            "type": "test_key",
            "name": "WebCria Test",
            "test_key": "************",
            "publishable_test_key": "************",
            "production_key": null,
            "publishable_production_key": null,
            "uri": "\/v1\/marketplaces\/dc89ed2081f2459f8bd55f2c051a7256",
            "metadata": {},
            "created_at": "2019-12-16T17:08:54+00:00",
            "updated_at": "2019-12-16T17:08:54+00:00"
        }
    ],
    "transfer_enabled": true,
    "transfer_policy": "daily",
    "debit_enabled": false,
    "default_debit": null,
    "default_credit": null,
    "id_parent_marketplace": null,
    "uri": "\/v1\/marketplaces\/2c75c5622aff4ba087e0e50c8d6d2919",
    "created_at": "2019-12-16T17:08:54+00:00",
    "updated_at": "2019-12-16T17:08:54+00:00",
    "metadata": []
}
*/

    public $name;
    public $type;
    public $description;
    public $is_active;
    public $is_verified;
    public $account_balance;
    public $current_balance;
    public $decline_on_fail_security_code;
    public $decline_on_fail_zip_code;
    public $support_email;
    public $phone_number;
    public $statement_descriptor;
    public $website;
    public $facebook;
    public $twitter;

    protected $http;
    
    /**
     * __construct
     *
     * @param  Http $http
     * @return void
     */
    public function __construct(Http $http)
    {
        $this->http = $http;
    }
    
    /**
     * deserialize
     *
     * @param  mixed $data
     * @return void
     */
    public function deserialize($data)
    {   
        foreach($data as $key => $value) {
            if (property_exists($this, $key) && $key != 'address') {
                $this->$key = $value;
            }
        }
    }
    
    /**
     * getSeller
     *
     * @param  mixed $id
     * @return $this
     */
    public function getMarketplace()
    {
        $response = $this->http->get('');
        $body = json_decode($response->getBody());
        $this->deserialize($body);

        return $this;
    }
    
    
}
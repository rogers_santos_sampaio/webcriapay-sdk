<?php

namespace Webcria\Webcriapay;

use Webcria\Webcriapay\Base;
use Webcria\Webcriapay\Comprador;
use Webcria\Webcriapay\ItemList;

class Compradores extends Base
{
    use ItemList;

    protected $http;
    protected $compradores = [];

    public function __construct(Http $http)
    {
        $this->http = $http;
    }

    public function searchComprador($document)
    {
        $query = 'taxpayer_id=' . $document;
        $response = $this->http->get('buyer/search?' . $query);
        $body = json_decode($response->getBody());
        $this->setObject($body);
        
    }

    public function listarCompradores($limit = 20, $sort = 'time-descending', $offset = 0)
    {
        $response = $this->http->get('buyers?limit='.$limit.'&sort='.$sort.'&offset='.$offset);
        $body = json_decode($response->getBody());
        $this->setObject($body);
    }

    private function setObject($object)
    {
        foreach($object as $key => $value) {
            if (property_exists($this, $key)) {
               $this->$key = $value;
            } else if ($key == 'items') {
                foreach ($value as $item) {
                    $this->setComprador($item);
                }
            }
        }
    }

    private function setComprador($item) 
    {
        $comprador = new Comprador($this->http);
        $comprador->deserialize($item);
        $this->compradores[] = $comprador;
    }

}
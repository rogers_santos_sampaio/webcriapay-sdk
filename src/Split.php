<?php
namespace Webcria\Webcriapay;

use Webcria\Webcriapay\Base;

class Split extends Base
{
    public $recipient;
    public $liable;
    public $amount;
    public $percentage;
    public $charge_processing_fee;


    public function toArray()
    {   
        return get_object_vars($this);
    }

}
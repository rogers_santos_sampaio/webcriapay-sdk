<?php

namespace Webcria\Webcriapay;

use Webcria\Webcriapay\Base;

class Webhook extends Base
{

    protected $http;
    public $id;
    public $method = 'POST';
    public $url;
    public $description;
    public $events; 

    public function __construct(Http $http)
    {
        $this->http = $http;
    }

    /**
     * deserialize
     *
     * @param  mixed $data
     * @return void
     */
    public function deserialize($data)
    {   

        foreach($data as $key => $value) {
            if (property_exists($this, $key)) {
                $this->$key = $value;
            }
        }

    }

    /**
     * Save Webhook
     *
     * @param  mixed $data
     * @return $id
     */
    public function saveWebhook()
    {   
        $data = [
            'method'        =>  $this->method,
            'url'           =>  $this->url,
            'description'   =>  $this->description,
            'event'        =>  $this->events
        ];
        
        $response = $this->http->post('webhooks', $data);
        $body = json_decode($response->getBody());
        $this->id = $body->id;

        return $this->id;

    }

    /**
     * get Webook
     *
     * @param  mixed $id
     * @return $this
     */
    public function getWebhook($id)
    {
        $response = $this->http->get('webhook/' . $id);
        $body = json_decode($response->getBody());
        $this->deserialize($body);

        return $this;
    }

    /**
     * delete Webhook
     *
     * @param  mixed $id
     * @return void
     */
    public function deleteWebhook($id)
    {
        $response = $this->http->delete('webhook/', $id);
        $body = json_decode($response->getBody());

        return $body;
    }

}
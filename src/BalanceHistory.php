<?php

namespace Webcria\Webcriapay;

use Webcria\Webcriapay\Base;

use Webcria\Webcriapay\Owner;
use Webcria\Webcriapay\Address;
use Webcria\Webcriapay\Business;
use Webcria\Webcriapay\ItemList;
use Webcria\Webcriapay\Vendedor;

class BalanceHistory extends Base
{
    use ItemList;

    protected $http;
    protected $items = [];

    public function __construct(Http $http)
    {
        $this->http = $http;
    }
    
    /**
     * getBalanceHistory
     *
     * @param  string $type
     * @return void
     */
    public function getBalanceHistory($type, $id, $limit = 100, $sort = 'time-descending', $offset = 0) 
    {
        $response = $this->http->get($type . '/'. $id .'/balances/history?limit='.$limit.'&sort='.$sort.'&offset='.$offset);
        $body = json_decode($response->getBody());
        $this->setObject($body);

        return $this;
    }

    private function setObject($object)
    {
        foreach($object as $key => $value) {
            if (property_exists($this, $key)) {
               $this->$key = $value;
            }
        }
    }

}
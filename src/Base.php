<?php

namespace Webcria\Webcriapay;

abstract class Base
{
    public function __get($property)
    {
        $getter = "get{$property}";
        if (property_exists($this, $property)) {
            return $this->$property;
        } else if (method_exists($this, $getter)) {
            return $this->$getter();
        } else {
            trigger_error("property '{$property}' is not found.");
        }
    }

    public function __set($property, $value)
    {
        $setter = "set{$property}";
        if (property_exists($this, $property)) {
            $this->$property = $value;
        } else if (method_exists($this, $setter)) {
            $this->$setter($value);
        } else {
            trigger_error("property '{$property}' is not found.");
        }
        
    }
}
<?php

namespace Webcria\Webcriapay;

use Webcria\Webcriapay\Base;

use Webcria\Webcriapay\Owner;
use Webcria\Webcriapay\Address;
use Webcria\Webcriapay\Business;
use Webcria\Webcriapay\Comprador;

use Webcria\Webcriapay\Http;

class Transaction extends Base
{

    public $id;
    public $reference_id;
    public $status;
    public $resource;
    public $amount;
    public $original_amount;
    public $currency;
    public $description;
    public $payment_type;
    public $transaction_number;
    public $sales_receipt;
    public $on_behalf_of;
    public $statement_descriptor;
    public $payment_method;
    public $point_of_sale;
    public $refunded;
    public $voided;
    public $captured;
    public $fees;
    public $source;
    public $metadata;
    public $expected_on;
    public $created_at;
    public $updated_at;


    protected $http;
    
    /**
     * __construct
     *
     * @param  Http $http
     * @return void
     */
    public function __construct(Http $http)
    {
        $this->http = $http;
    }
    
    /**
     * deserialize
     *
     * @param  mixed $data
     * @return void
     */
    public function deserialize($data)
    {   
        foreach ($data as $property => $value) {

            if(property_exists($this, $property)) {
                $this->$property = $value;
            }
        }
    }

    public function oneTime($reference_id, $token, $valor, $vendedor, $optional, $capture = true)
    {
        $data = [
            'reference_id'  => $reference_id,
            'token'         => $token,
            'on_behalf_of'  => $vendedor,
            'amount'        => $valor,
            'capture'       => $capture  
        ];

        foreach ($optional as $key => $item) {
            $data[$key] = $item;
        }

        var_dump($data);

        $response = $this->http->post('transactions/', $data);
        $body = json_decode($response->getBody());
        
        $this->deserialize($body);

        return $this->id;
    }

    public function criar($reference_id, $comprador_id, $valor, $vendedor, $optional, $capture = true)
    {
        $data = [
            'reference_id'  => $reference_id,
            'customer'      => $comprador_id,
            'on_behalf_of'  => $vendedor,
            'amount'        => $valor,
            'capture'       => $capture  
        ];

        foreach ($optional as $key => $item) {
            $data[$key] = $item;
        }

        $data['source'] = $this->source;

        $response = $this->http->post('transactions/', $data);
        $body = json_decode($response->getBody());
        
        $this->deserialize($body);

        return $this->id;
    }

    public function estornar($id, $valor, $vendedor)
    {   
        $data = [
            'on_behalf_of'  => $vendedor,
            'amount'        => $valor
        ];

        $response = $this->http->post('transactions/' . $id . '/void', $data);
        $body = json_decode($response->getBody());

        $this->deserialize($body);

        return $this->id;
    }

    public function capturar($id, $valor, $vendedor)
    {   
        $data = [
            'on_behalf_of'  => $vendedor,
            'amount'        => $valor
        ];

        $response = $this->http->post('transactions/' . $id . '/capture', $data);
        $body = json_decode($response->getBody());

        $this->deserialize($body);

        return $this->id;
    }

    public function addSource($card_id)
    {

        $this->source = [
                'type'  => 'card',
                'card'  => [
                    'id' => $card_id
                ]
            ];

    }
    
    
}
<?php

use PHPUnit\Framework\TestCase;
use Webcria\Webcriapay\Address;
use Webcria\Webcriapay\Business;
use Webcria\Webcriapay\Owner;
use Webcria\Webcriapay\Split;
use Webcria\Webcriapay\Webcriapay;

use function PHPUnit\Framework\assertJson;

class WebCriapayTests extends TestCase
{
    public function testInstantiaion()
    {
        $obj = new Webcriapay('zpk_test_f1srQUSvqvORqgSr1F1Ttuqb', '2c75c5622aff4ba087e0e50c8d6d2919');
        $this->assertInstanceOf('\Webcria\Webcriapay\Webcriapay', $obj);
    }

    public function testMarketplace()
    {
        $obj = new Webcriapay('zpk_test_f1srQUSvqvORqgSr1F1Ttuqb', '2c75c5622aff4ba087e0e50c8d6d2919');
        $marketplace = $obj->marketplace();
        $marketplace->getMarketplace();
        $this->assertEquals('85999959122', $marketplace->phone_number, 'The value is ' . $marketplace->phone_number);
    }

    public function testListarVendedores()
    {
        $obj = new Webcriapay('zpk_test_f1srQUSvqvORqgSr1F1Ttuqb', '2c75c5622aff4ba087e0e50c8d6d2919');
        $vendedores = $obj->vendedores();
        $vendedores->listarVendedores();
        $total_pages = $vendedores->total_pages;
        $this->assertEquals(18, count($vendedores->vendedores), 'The value is ' . count($vendedores->vendedores));
    }

    public function testListarMcc()
    {
        $obj = new Webcriapay('zpk_test_f1srQUSvqvORqgSr1F1Ttuqb', '2c75c5622aff4ba087e0e50c8d6d2919');
        $mcc = $obj->mcc();
        $mcc->list();
        $total = $mcc->total;
        $this->assertEquals(104, $total, 'The value is ' . $total);
    }

    public function testCriarOwner()
    {
        $faker = Faker\Factory::create('pt_BR');

        $owner = new Owner();
        $owner->first_name = $faker->firstName;
        $owner->last_name = $faker->lastName;
        $owner->email = $faker->email;
        $owner->website = 'https://allur.co';
        $owner->phone_number = '85991771000';
        $owner->taxpayer_id = $faker->cpf(false);
        $owner->birthdate = '1976-04-13';
        $owner->address = new Address();
        $owner->address->line1 = 'Av. dos Expedicionários';
        $owner->address->line2 = 'Bloco 10 Apt 303';
        $owner->address->neighborhood = 'Vila União';
        $owner->address->city = 'Fortaleza';
        $owner->address->state = 'Ceará';
        $owner->address->postal_code = '60410-234';
        $owner->address->country_code = 'BR';
        $this->assertEquals('https://allur.co', $owner->website, 'The value is ' . $owner->website);
    }

    public function testCriarVendedorIndividual()
    {
        $faker = Faker\Factory::create('pt_BR');
        
        $obj = new Webcriapay('zpk_test_f1srQUSvqvORqgSr1F1Ttuqb', '2c75c5622aff4ba087e0e50c8d6d2919');
        $vendedor = $obj->vendedor();
        $vendedor->owner = new Owner();
        $vendedor->owner->first_name = $faker->firstName;
        $vendedor->owner->last_name = $faker->lastName;
        $vendedor->owner->email = $faker->email;
        $vendedor->owner->website = 'https://allur.co';
        $vendedor->owner->phone_number = '85991771000';
        $vendedor->owner->taxpayer_id = $faker->cpf(false);
        $vendedor->owner->birthdate = '1976-04-13';
        $vendedor->owner->address = new Address();
        $vendedor->owner->address->line1 = 'Av. dos Expedicionários';
        $vendedor->owner->address->line2 = 'Bloco 10 Apt 303';
        $vendedor->owner->address->neighborhood = 'Vila União';
        $vendedor->owner->address->city = 'Fortaleza';
        $vendedor->owner->address->state = 'CE';
        $vendedor->owner->address->postal_code = '60410-234';
        $vendedor->owner->address->country_code = 'BR';
        $vendedor->owner_address = $vendedor->owner->address;
        $vendedor->statement_descriptor = '';
        $vendedor->description = '';
        $vendedor->metadata = [];
        $vendedor->parent_seller = '';
        $vendedor->mcc = "1";
        
        $id = $vendedor->createIndividual();

        $this->assertEquals('individual', $vendedor->type, 'The value is ' . $vendedor->type);
    }

    public function testCriarVendedorBusiness()
    {
        $faker = Faker\Factory::create('pt_BR');
        
        $obj = new Webcriapay('zpk_test_f1srQUSvqvORqgSr1F1Ttuqb', '2c75c5622aff4ba087e0e50c8d6d2919');
        $vendedor = $obj->vendedor();
        $vendedor->owner = new Owner();
        $vendedor->owner->first_name = $faker->firstName;
        $vendedor->owner->last_name = $faker->lastName;
        $vendedor->owner->email = $faker->email;
        $vendedor->owner->website = 'https://allur.co';
        $vendedor->owner->phone_number = '85991771000';
        $vendedor->owner->taxpayer_id = $faker->cpf(false);
        $vendedor->owner->birthdate = '1976-04-13';
        $vendedor->owner->address = new Address();
        $vendedor->owner->address->line1 = 'Av. dos Expedicionários';
        $vendedor->owner->address->line2 = 'Bloco 10 Apt 303';
        $vendedor->owner->address->neighborhood = 'Vila União';
        $vendedor->owner->address->city = 'Fortaleza';
        $vendedor->owner->address->state = 'CE';
        $vendedor->owner->address->postal_code = '60410-234';
        $vendedor->owner->address->country_code = 'BR';
        $vendedor->owner_address = $vendedor->owner->address;
        $vendedor->business_address = $vendedor->owner->address;

        $vendedor->business = new Business();
        $vendedor->business->name = 'Test One Business Name';
        $vendedor->business->phone = '+5585991771000';
        $vendedor->business->email = $faker->email; 
        $vendedor->business->website = 'https://allur.co'; 
        $vendedor->business->description = 'Um negocio legal';
        $vendedor->business->facebook = '';
        $vendedor->business->twitter = '';
        $vendedor->business->ein = $faker->cnpj(false);
        $vendedor->business->business_opening_date = '2002-01-14';


        $vendedor->statement_descriptor = '';
        $vendedor->description = '';
        $vendedor->metadata = [];
        $vendedor->parent_seller = '';
        $vendedor->mcc = "1";
        
        $id = $vendedor->createBusinesses();

        $this->assertEquals('business', $vendedor->type, 'The value is ' . $vendedor->type);
    }

    public function testGetVendedor()
    {
        $id = '5275cfe2e91b4b0486209cb90814b7a8';
        $obj = new Webcriapay('zpk_test_f1srQUSvqvORqgSr1F1Ttuqb', '2c75c5622aff4ba087e0e50c8d6d2919');
        $vendedor = $obj->vendedor();
        $vendedor->getVendedor($id);

        $this->assertEquals('individual', $vendedor->type, 'The value is ' . $vendedor->type);
    }

    public function testSalvarVendedorIndividual()
    {
        $id = '5275cfe2e91b4b0486209cb90814b7a8';
        $obj = new Webcriapay('zpk_test_f1srQUSvqvORqgSr1F1Ttuqb', '2c75c5622aff4ba087e0e50c8d6d2919');
        $vendedor = $obj->vendedor();
        $vendedor->getVendedor($id);
        $vendedor->owner->phone_number = '85991771001';
        $vendedor->saveIndividual();
        $vendedor->getVendedor($id);

        $this->assertEquals('85991771001', $vendedor->owner->phone_number, 'The value is ' . $vendedor->owner->phone_number);

    }

    public function testSalvarVendedorBusiness()
    {
        $id = '37487ada779a42b5b43b96322d4570e1';
        $obj = new Webcriapay('zpk_test_f1srQUSvqvORqgSr1F1Ttuqb', '2c75c5622aff4ba087e0e50c8d6d2919');
        $vendedor = $obj->vendedor();
        $vendedor->getVendedor($id);
        $vendedor->business->phone = '85991771002';
        $vendedor->saveBusiness();
        $vendedor->getVendedor($id);

        $this->assertEquals('85991771002', $vendedor->business->phone, 'The value is ' . $vendedor->owner->phone_number);

    }

    public function testCriarComprador() 
    {   

        $faker = Faker\Factory::create('pt_BR');
        $venderdor_id = '5275cfe2e91b4b0486209cb90814b7a8';
        $obj = new Webcriapay('zpk_test_f1srQUSvqvORqgSr1F1Ttuqb', '2c75c5622aff4ba087e0e50c8d6d2919');
        $comprador = $obj->comprador();
        $comprador->parent_seller = $venderdor_id;
        $comprador->first_name = $faker->firstName;
        $comprador->last_name = $faker->lastName;
        $email = $faker->email;
        $comprador->email = $email;
        $comprador->phone_number = '85991771000';
        $comprador->taxpayer_id = $faker->cpf(false);
        $comprador->birthdate = '1976-04-13';
        $comprador->address = new Address();
        $comprador->address->line1 = 'Av. dos Expedicionários';
        $comprador->address->line2 = 'Bloco 10 Apt 303';
        $comprador->address->neighborhood = 'Vila União';
        $comprador->address->city = 'Fortaleza';
        $comprador->address->state = 'CE';
        $comprador->address->postal_code = '60410-234';
        $comprador->address->country_code = 'BR';
        $comprador->description = '';

        $id = $comprador->createComprador();

        $this->assertEquals($email, $comprador->email, 'The value is ' . $comprador->email);
    }

    public function testGetComprador()
    {
        $id = 'a7d1e5fe263a49979237b69bfedf9f66';
        $obj = new Webcriapay('zpk_test_f1srQUSvqvORqgSr1F1Ttuqb', '2c75c5622aff4ba087e0e50c8d6d2919');
        $comprador = $obj->comprador();
        $comprador = $comprador->get($id);

        $this->assertEquals('gmontenegro@ferreira.org', $comprador->email, 'The value is ' . $comprador->email);
    }

    public function testAttachCard()
    {
        $id = 'a7d1e5fe263a49979237b69bfedf9f66';
        $token = 'b3ce3ab419314e029a641490b50e9cfc';
        $obj = new Webcriapay('zpk_test_f1srQUSvqvORqgSr1F1Ttuqb', '2c75c5622aff4ba087e0e50c8d6d2919');
        $card = $obj->card();
        $card->token = $token;
        $card->associar($id);

        $this->assertEquals('3a627e982728427eab6e4d8fe301eaec', $card->id, 'The value is ' . $card->id);
    }

    public function testTransactionOneTime()
    {
        $faker = Faker\Factory::create('pt_BR');
        $token = '09c62162f86d4244b207acd010380571';
        $seller = '19e54a89b94d47afb00ed5a4c99380f6';
        $webcria = '0b5c794ef0c8410683562279c8b2bb8b';
        $obj = new Webcriapay('zpk_test_f1srQUSvqvORqgSr1F1Ttuqb', '2c75c5622aff4ba087e0e50c8d6d2919');
        $transaction = $obj->transaction();
        $extra = [
            'description' => "venda",
            'statement_descriptor' => "LOJA JOAO"
        ];
        $transaction->oneTime($faker->uuid, $token, 7, $seller, $extra, true);  

        $this->assertEquals('string', gettype($transaction->id), 'The value is ' . $transaction->id);
    }

    public function testTransactionClienteCapture()
    {
        $faker = Faker\Factory::create('pt_BR');
        $card_id = '3a627e982728427eab6e4d8fe301eaec';
        $customer = 'a7d1e5fe263a49979237b69bfedf9f66';
        $seller = '19e54a89b94d47afb00ed5a4c99380f6';
        $webcria = '0b5c794ef0c8410683562279c8b2bb8b';
        $obj = new Webcriapay('zpk_test_f1srQUSvqvORqgSr1F1Ttuqb', '2c75c5622aff4ba087e0e50c8d6d2919');
        $transaction = $obj->transaction();
        $extra = [
            'description' => "venda",
            'statement_descriptor' => "LOJA JOAO"
        ];
        $transaction->addSource($card_id);
        $transaction->criar($faker->uuid, $customer, 7, $seller, $extra, true);  

        $this->assertEquals('string', gettype($transaction->id), 'The value is ' . $transaction->id);
    }

    public function testTransactionClienteApprove()
    {
        $faker = Faker\Factory::create('pt_BR');
        $card_id = '3a627e982728427eab6e4d8fe301eaec';
        $customer = 'a7d1e5fe263a49979237b69bfedf9f66';
        $seller = '19e54a89b94d47afb00ed5a4c99380f6';
        $webcria = '0b5c794ef0c8410683562279c8b2bb8b';
        $obj = new Webcriapay('zpk_test_f1srQUSvqvORqgSr1F1Ttuqb', '2c75c5622aff4ba087e0e50c8d6d2919');
        $transaction = $obj->transaction();
        $extra = [
            'description' => "venda",
            'statement_descriptor' => "LOJA JOAO"
        ];
        $transaction->addSource($card_id);
        $transaction->criar($faker->uuid, $customer, 7, $seller, $extra, false);  
        var_dump($transaction);   

        $this->assertEquals('string', gettype($transaction->id), 'The value is ' . $transaction->id);
    }

    public function testCaptureTransaction()
    {   
        $id = 'd2a9d808d75b4d1499506b67c692a7ae';
        $seller = '19e54a89b94d47afb00ed5a4c99380f6';
        $obj = new Webcriapay('zpk_test_f1srQUSvqvORqgSr1F1Ttuqb', '2c75c5622aff4ba087e0e50c8d6d2919');
        $transaction = $obj->transaction();
        $transaction->capturar($id, 7, $seller);  
        var_dump($transaction);   

        $this->assertEquals('string', gettype($transaction->id), 'The value is ' . $transaction->id);
    }

    public function testRefundTransaction()
    {   
        $id = 'd2a9d808d75b4d1499506b67c692a7ae';
        $seller = '19e54a89b94d47afb00ed5a4c99380f6';
        $obj = new Webcriapay('zpk_test_f1srQUSvqvORqgSr1F1Ttuqb', '2c75c5622aff4ba087e0e50c8d6d2919');
        $transaction = $obj->transaction();
        $transaction->estornar($id, 7, $seller);  
        var_dump($transaction);   

        $this->assertEquals('string', gettype($transaction->id), 'The value is ' . $transaction->id);
    }

    public function testTransactionClienteSplit()
    {
        $faker = Faker\Factory::create('pt_BR');
        $card_id = '3a627e982728427eab6e4d8fe301eaec';
        $customer = 'a7d1e5fe263a49979237b69bfedf9f66';
        $seller = '19e54a89b94d47afb00ed5a4c99380f6';
        $webcria = '0b5c794ef0c8410683562279c8b2bb8b';
        $obj = new Webcriapay('zpk_test_f1srQUSvqvORqgSr1F1Ttuqb', '2c75c5622aff4ba087e0e50c8d6d2919');
        $transaction = $obj->transaction();
        
        $split = new Split();
        $split->recipient = $webcria;
        $split->liable = true;
        $split->amount = 0;
        $split->percentage = 1;
        $split->charge_processing_fee = false;

        $extra = [
            'description' => "venda",
            'statement_descriptor' => "LOJA JOAO",
            'split_rules' => [$split]
        ];

        $transaction->addSource($card_id);
        $transaction->criar($faker->uuid, $customer, 207, $seller, $extra, true);  
        var_dump($transaction);   

        $this->assertEquals('string', gettype($transaction->id), 'The value is ' . $transaction->id);
    }

    public function testListarWebhooks()
    {
        $id = '5275cfe2e91b4b0486209cb90814b7a8';
        $obj = new Webcriapay('zpk_test_f1srQUSvqvORqgSr1F1Ttuqb', '2c75c5622aff4ba087e0e50c8d6d2919');
        $webhooks = $obj->webhooks();
        $webhooks->listarWebhooks();

        $this->assertEquals(0, count($webhooks->webhooks), 'The value is ' . count($webhooks->webhooks));

    }

    public function testCriaWebhook() 
    {   
 
        $obj = new Webcriapay('zpk_test_f1srQUSvqvORqgSr1F1Ttuqb', '2c75c5622aff4ba087e0e50c8d6d2919');
        $webhook = $obj->webhook();
        $webhook->url = 'https://asssassssassasasassass';
        $webhook->description = 'Webhook Legal';
        $webhook->events = [
            'seller.activated'
        ];

        $id = $webhook->saveWebhook();

        $webhooks = $obj->webhooks();
        $webhooks->listarWebhooks();

        $this->assertEquals(3, count($webhooks->webhooks), 'The value is ' . count($webhooks->webhooks));
    }


}